# The CERBERE Exercise

In this folder you find the documents and instructions necessary to replay your own version of the red team part for the CERBERE exercise.

All scenario related information is located in the corresponding folder, and some configuration files are provided in the configs folder as well.

In order to deploy the exercise locally you can follow the instructions [over here](https://gitlab.inria.fr/cidre-public/ursid).
